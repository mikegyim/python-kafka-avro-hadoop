import React, { useState } from 'react';
import axios from 'axios';

function App() {
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [id, setId] = useState('');
  const [hdfsData, setHdfsData] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  }

  const handleAgeChange = (event) => {
    setAge(event.target.value);
  }

  const handleIdChange = (event) => {
    setId(event.target.value);
  }

  const handleKafkaSubmit = async (event) => {
    event.preventDefault();
    const message = {
      id,
      name,
      age: parseInt(age)
    };
    await axios.post('/produce', message);
  }

  const handleHdfsSubmit = async (event) => {
    event.preventDefault();
    const data = {
      id,
      name,
      age: parseInt(age)
    };
    await axios.post('/write', data);
  }

  const handleHdfsRead = async (event) => {
    event.preventDefault();
    const response = await axios.get('/read');
    setHdfsData(response.data);
  }

  const handleKafkaRead = async (event) => {
    event.preventDefault();
    const response = await axios.get('/consume');
    console.log(response.data);
  }

  return (
    <div>
      <form onSubmit={handleKafkaSubmit}>
        <label>
          ID:
          <input type="text" value={id} onChange={handleIdChange} />
        </label>
        <br />
        <label>
          Name:
          <input type="text" value={name} onChange={handleNameChange} />
        </label>
        <br />
        <label>
          Age:
          <input type="text" value={age} onChange={handleAgeChange} />
        </label>
        <br />
        <button type="submit">Produce to Kafka</button>
      </form>
      <br />
      <button onClick={handleKafkaRead}>Consume from Kafka</button>
      <br />
      <form onSubmit={handleHdfsSubmit}>
        <label>
          ID:
          <input type="text" value={id} onChange={handleIdChange} />
        </label>
        <br />
        <label>
          Name:
          <input type="text" value={name} onChange={handleNameChange} />
        </label>
        <br />
        <label>
          Age:
          <input type="text" value={age} onChange={handleAgeChange} />
        </label>
        <br />
        <button type="submit">Write to HDFS</button>
      </form>
      <br />
      <button onClick={handleHdfsRead}>Read from HDFS</button>
      <br />
      <pre>{JSON.stringify(hdfsData, null, 2)}</pre>
    </div>
  );
}

export default App;
