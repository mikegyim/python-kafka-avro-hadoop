from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer, AvroConsumer
from confluent_kafka.admin import AdminClient, NewTopic
from flask import Flask, request, jsonify
from flask_cors import CORS
import json
from hdfs import InsecureClient

app = Flask(__name__)
CORS(app)

# Define Avro schema
SCHEMA = """
{
    "namespace": "example.avro",
    "type": "record",
    "name": "User",
    "fields": [
        {"name": "id", "type": "string"},
        {"name": "name", "type": "string"},
        {"name": "age", "type": "int"}
    ]
}
"""

# Define Kafka configuration
KAFKA_CONFIG = {
    'bootstrap.servers': 'localhost:9092',
    'schema.registry.url': 'http://localhost:8081'
}

# Define HDFS configuration
HDFS_CONFIG = {
    'url': 'http://localhost:50070',
    'user': 'hadoop'
}

# Create Kafka topics
admin = AdminClient(KAFKA_CONFIG)
new_topic = NewTopic('test_topic', num_partitions=1, replication_factor=1)
admin.create_topics([new_topic])

# Define Avro producer
producer = AvroProducer(KAFKA_CONFIG, default_value_schema=avro.loads(SCHEMA))

# Define Avro consumer
consumer = AvroConsumer(
    KAFKA_CONFIG,
    {'group.id': 'test_group'},
    schema_registry=KAFKA_CONFIG['schema.registry.url']
)
consumer.subscribe(['test_topic'])

# Define HDFS client
client = InsecureClient(**HDFS_CONFIG)

@app.route('/produce', methods=['POST'])
def produce_message():
    message = request.json
    producer.produce(topic='test_topic', value=message)
    producer.flush()
    return jsonify({'success': True}), 200

@app.route('/consume', methods=['GET'])
def consume_message():
    messages = []
    while True:
        msg = consumer.poll(1.0)
        if msg is None:
            continue
        if msg.error():
            if msg.error().code() == avro.KafkaError._PARTITION_EOF:
                break
            else:
                print(f"Consumer error: {msg.error()}")
                continue
        messages.append(msg.value())
    return jsonify(messages)

@app.route('/write', methods=['POST'])
def write_to_hdfs():
    data = request.json
    with client.write('/test_file.txt', encoding='utf-8', overwrite=True) as writer:
        json.dump(data, writer)
    return jsonify({'success': True}), 200

@app.route('/read', methods=['GET'])
def read_from_hdfs():
    with client.read('/test_file.txt', encoding='utf-8') as reader:
        data = json.load(reader)
    return jsonify(data)

if __name__ == '__main__':
    app.run(debug=True)
