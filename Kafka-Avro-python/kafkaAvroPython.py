from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer, AvroConsumer
from uuid import uuid4

# Set up Avro schema for messages
schema = avro.load('path/to/schema.avsc')

# Set up Avro producer
producer = AvroProducer({
    'bootstrap.servers': 'localhost:9092',
    'schema.registry.url': 'http://localhost:8081'
}, default_value_schema=schema)

# Produce an Avro message to a Kafka topic
message = {"id": str(uuid4()), "name": "John Doe", "age": 30}
producer.produce(topic='test_topic', value=message)

# Set up Avro consumer
consumer = AvroConsumer({
    'bootstrap.servers': 'localhost:9092',
    'schema.registry.url': 'http://localhost:8081',
    'group.id': 'test_group',
    'auto.offset.reset': 'earliest'
}, schema_registry=avro.SchemaRegistryClient({'url': 'http://localhost:8081'}))

# Consume Avro messages from Kafka topic
consumer.subscribe(['test_topic'])
while True:
    message = consumer.poll(1.0)
    if message is None:
        continue
    if message.error():
        print(f"Error: {message.error()}")
        continue
    print(f"Received message: {message.value()}")
