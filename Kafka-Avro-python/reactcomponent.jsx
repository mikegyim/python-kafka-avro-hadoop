import React, { useState } from 'react';
import { AvroProducer, AvroConsumer } from 'confluent-avro-react';

const KafkaInterface = () => {
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  // Handle change to input field
  const handleInputChange = (event) => {
    setMessage(event.target.value);
  };

  // Handle producing a message to Kafka
  const handleProduce = async () => {
    try {
      const producer = new AvroProducer({
        schemaRegistry: 'http://localhost:8081',
        bootstrapServers: 'localhost:9092'
      });
      const message = {"id": "12345", "name": "John Doe", "age": 30};
      await producer.produce({ topic: 'test_topic', message });
      setMessage('');
    } catch (error) {
      console.error(error);
    }
  };

  // Handle consuming messages from Kafka
  const handleConsume = async () => {
    try {
      const consumer = new AvroConsumer({
        schemaRegistry: 'http://localhost:8081',
        bootstrapServers: 'localhost:9092',
        groupId: 'test_group',
        topic: 'test_topic',
        onMessage: (message) => {
          setMessages((prevMessages) => [...prevMessages, message]);
        }
      });
      await consumer.consume();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1>Kafka Interface</h1>
      <div>
        <label>Message:</label>
        <input type="text" value={message} onChange={handleInputChange} />
        <button onClick={handleProduce}>Produce</button>
      </div>
      <div>
        <button onClick={handleConsume}>Consume</button>
        <ul>
          {messages.map((message, index) => (
            <li key={index}>{JSON.stringify(message)}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default KafkaInterface;
