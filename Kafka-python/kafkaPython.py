from kafka import KafkaProducer, KafkaConsumer

# Set up Kafka producer
producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

# Produce a message to a Kafka topic
producer.send('test_topic', b'Hello, Kafka!')

# Set up Kafka consumer
consumer = KafkaConsumer('test_topic', bootstrap_servers=['localhost:9092'], auto_offset_reset='earliest')

# Consume messages from Kafka topic
for message in consumer:
    print(f"Received message: {message.value.decode('utf-8')}")
